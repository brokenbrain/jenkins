import Koa from 'koa'
import http from 'http'
import {promisify} from 'util'

const app = new Koa()
http.createServer(app.callback())
app.use(async (ctx, next) => {
    ctx.body = {msg: 'ok', status: 200}
})
;(async () => {
    const port = 8000;
    await promisify(app.listen).call(app, port, undefined)
    console.log(`server open on port ${port}`)
})()