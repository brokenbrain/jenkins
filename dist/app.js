"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_1 = __importDefault(require("koa"));
const http_1 = __importDefault(require("http"));
const util_1 = require("util");
const app = new koa_1.default();
http_1.default.createServer(app.callback());
app.use((ctx, next) => __awaiter(this, void 0, void 0, function* () {
    ctx.body = { msg: 'ok' };
}));
(() => __awaiter(this, void 0, void 0, function* () {
    const port = 8000;
    yield util_1.promisify(app.listen).call(app, port, undefined);
    console.log(`server open on port ${port}`);
}))();
//# sourceMappingURL=app.js.map